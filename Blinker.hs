module Blinker where

import Clash.Prelude
import Data.Word

{-# NOINLINE topEntity #-}
{-# ANN topEntity
  (Synthesize
    { t_name   = "Blinker"
    , t_inputs = [PortName "clk", PortName "reset"]
    , t_output = PortProduct "" [PortName "D9", PortName "D8"]
    }) #-}

topEntity :: Clock System Source
          -> Reset System Asynchronous
          -> (Signal System Bit, Signal System Bit)
topEntity clk rst = (exposeClockReset (checker <$> upCounter) clk rst, exposeClockReset (checker <$> upCounter) clk rst)
------

checker :: (Unsigned 22) -> Bit
checker n = on
  where
  m = 2048576
  on | n <= m = high
     | otherwise = low

upCounter :: (HiddenClockReset System gated synchronous) => Signal System (Unsigned 22)
upCounter = s
  where
  s = register 0 (s+1)

{-
topEntity
  :: Clock System Source
  -> Reset System Asynchronous
  -> Signal System Bit
topEntity = exposeClockReset $ tickTock m
  where
    m = 6400000 :: (Unsigned 32)

tickTock :: (HiddenClockReset domain gated synchronous) => (Unsigned 32) -> Signal domain Bit
tickTock n = mealy step (False, 0) dodo
  where
    dodo :: Signal domain (Bool, Int)
    dodo = pure (False,0)
    step (s, k) _ =
        let k' = k + 1
            finished = k' == n
            s' = if finished then not s else s
            k'' = if finished then 0 else k'
        in ((s', k''), if s' then high else low)
-}

