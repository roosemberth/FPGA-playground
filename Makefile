#Makefile for the FPGA LATTICE ICE40 8K
TARGET := Blinker

all: compile upload

compile:
	~/.cabal/bin/clash --verilog $(TARGET).hs

upload: $(TARGET).bin
	iceprog $(TARGET).bin

$(TARGET).bin:
	yosys -p "synth_ice40 -blif $(TARGET).blif" "./verilog/$(TARGET)/$(TARGET)/$(TARGET).v"
	arachne-pnr -d 8k -p 8k.pcf $(TARGET).blif -o $(TARGET).asc
	icepack $(TARGET).asc $(TARGET).bin

clean:
	rm -f $(TARGET).blif $(TARGET).txt $(TARGET).ex $(TARGET).bin
	rm -rf verilog vhdl
